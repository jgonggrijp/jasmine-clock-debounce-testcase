const gulp = require('gulp');
const browserify = require('browserify');
const vinylStream = require('vinyl-source-stream');
const vinylBuffer = require('vinyl-buffer');
const watchify = require('watchify');
const plugins = require('gulp-load-plugins')();

const unittestBundleName = 'bundle.js',
    unittestEntries = ['test.js'],
    ports = {frontend: 8080, unittest: 8088};

const testModules = browserify({
    debug: true,
    entries: unittestEntries,
    cache: {},
    packageCache: {},
});

exports.test = test;
function test() {
    return testModules.bundle()
        .pipe(vinylStream(unittestBundleName))
        .pipe(vinylBuffer())
        .pipe(gulp.dest('.'))
        .pipe(plugins.connect.reload())
        .pipe(plugins.jasmine({errorOnFail: false}));
}

exports.serve = serve;
function serve() {
    plugins.connect.server({
        port: ports.frontend,
        name: 'test',
        root: '.',
        livereload: true,
    });
};

exports.watch = watch;
function watch() {
    testModules.plugin(watchify);
    testModules.on('update', test);
    test();
};

exports.default = gulp.parallel(watch, serve);
