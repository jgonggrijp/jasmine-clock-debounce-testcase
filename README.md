# Jasmine.clock() + lodash.debounce testcase

*Fixed as of the latest commit. In order for `lodash.debounce` to work correctly with a mocked clock, you need to call `jasmine.clock().install().mockDate()` rather than just `jasmine.clock().install()`.*

This is a reduced testcase, extracted from a web application where I ran into a testing issue. In the affected test, a call to a debounced function is handled synchronously using Jasmine's mock clock. The test consistently fails to display the expected behaviour when run normally. However, when stepping through the test with a debugger, the test succeeds. This makes it (almost?) impossible to assess what is going wrong.

The test code is contained in the `test.ts`. Run `yarn` to install the dependencies and then `yarn gulp` to run the tests. This will run the tests one time and then run them again on each code change.

The code includes a comment indicating where to put a breakpoint. If you set a breakpoint here, run the tests, let execution pause at the breakpoint and then allow execution to continue again, the affected test will pass. You can do this in the browser by visiting http://localhost:8080/specRunner.html. A source map is included. This page will also livereload on every code change.

Curiously, this problem occurs with lodash's version of `_.debounce` but not with underscore's version. Both use `setTimeout` and `clearTimeout` under the hood. One apparent difference is that the lodash implementation explicitly manipulates the numerical value of the current time while underscore does not. I am not sure, however, whether this is causing the discrepancy.
