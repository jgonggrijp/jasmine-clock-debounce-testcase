const lodash = require('lodash');
const underscore = require('underscore');

describe('jasmine.clock', function() {
    const timeout = 200;

    beforeEach(function() {
        jasmine.clock().install().mockDate();
    });

    afterEach(function() {
        jasmine.clock().uninstall();
    });

    it('interacts strangely with lodash.debounce', function() {
        let state = 'initial';
        const trigger = lodash.debounce(transition, timeout);
        trigger();
        jasmine.clock().tick(timeout - 1);
        expect(state).toBe('initial');
        // Insert breakpoint here to make the test pass.
        jasmine.clock().tick(2);
        expect(state).toBe('final');

        function transition() {
            state = 'final';
        }
    });

    it('interacts as expected with underscore.debounce', function() {
        let state = 'initial';
        const trigger = underscore.debounce(transition, timeout);
        trigger();
        jasmine.clock().tick(timeout - 1);
        expect(state).toBe('initial');
        jasmine.clock().tick(2);
        expect(state).toBe('final');

        function transition() {
            state = 'final';
        }
    });
});
